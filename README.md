# The Simple calculator API

This is a simple calculator API which is developed using Ruby On Rails. This API will allow the users to perform addition, subtraction, multiplication , division and module of two numbers.

## Requirements

Ruby >= 3.0.0
 
Rails >= 7.0.3.1

Docker

Postman/Insomnia/Curl(to call the API)

## How to run the project in docker

Docker image of this project is already created and deployed in hub.docker.com
https://hub.docker.com/r/sshamsudheen/ror_calc, pull the docker image to your local and then run the container from the image sshamsudheen/ror_calc.

```bash
docker pull sshamsudheen/ror_calc
docker run -p 3001:3000 sshamsudheen/ror_calc
```

## Usage

CLI
```bash
curl --header "Content-type: application/json" --request POST --data '{"number1":15, "number2":10, "operation":"*"}' http://localhost:3001/calcs
```
Insomnia / Postman
```bash
POST API Endpoint: http://localhost:3001/calcs
post request body:
{"number1":15, "number2":6, "operation":"+"}

```

## Validation
API validates the request body, it should have variables number1, number2 and operation.

Variables number1 and number2 should be numbers and operation variable should be either one of [+,-,*,/,%]

## PIPELINES

This project pipeline has 3 jobs `Build, Test and Deploy`

Whenever changes made in a feature/own branch and pushed to own branch the job `Build` and `Test` will be executed, if it succeeds and merged to main branch 'shams', then `Deploy` job will be executed along with `Build and Test` which will deploy the image to dockerhub.

###### Improvements
As of now jobs `Build and Test` are not doing anything real, more unit test need to be added in Test job and Build job too needed some real thing to perform. 

## API

The directory `calculator` has the rails project for calculator API. Whenever the API endpoint is called it calls the method in CalcsController in controller (https://gitlab.com/sshamsudheen/ror/-/blob/shams/calculator/app/controllers/calcs_controller.rb#L19)

There is also Validation class in lib/ which will take care of the validation and CalculatorLib which will take care of calculations like addition, subtraction, multiplication , division and module between 2 numbers.