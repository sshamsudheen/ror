class CalcsController < ApplicationController
  require 'calculator_lib'
  require 'validation'
  before_action :set_calc, only: %i[ show update destroy ]

  # GET /calcs
  def index
    @calcs = Calc.all

    render json: @calcs
  end

  # GET /calcs/1
  def show
    render json: @calc
  end

  # POST /calcs
  def create

    begin
      # validate the given input
      is_valid_data, operator, err_msg = Validation.validate_data(params)
    
      if is_valid_data && operator!= ""
        @result = CalculatorLib.send(operator, *[params[:number1], params[:number2]])
        render :json => @result
      else
        render :json => {"status": "error", "message": err_msg}
      end
    rescue => err
      Rails.logger.error 'Error in parameter body'
      render :json => {"status": "error", "message": err}
    end
    
  end

  # PATCH/PUT /calcs/1
  def update
    if @calc.update(calc_params)
      render json: @calc
    else
      render json: @calc.errors, status: :unprocessable_entity
    end
  end

  # DELETE /calcs/1
  def destroy
    @calc.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_calc
      @calc = Calc.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def calc_params
      params.require(:calc).permit(:number1, :number2, :operation)
    end
end
