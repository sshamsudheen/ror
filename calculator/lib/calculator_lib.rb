class CalculatorLib
  @status = 'success'

  # Method to perform addition
  def self.sum(a, b)
    Rails.logger.info "Performing addition between #{a} and # #{b}"
    begin 
      result = a + b
    rescue => err
      result = "cannot perform addition #{err}"
      @status = "failure"
    end

    return generate_response(result, @status)
  end

  # Method to perform subtraction
  def self.subtract(a, b)
    Rails.logger.info "Performing subtraction between #{a} and # #{b}"
    begin
      result = a - b
    rescue => err
      result = "cannot perform subtraction #{err}"
      @status = "failure"
    end
    return generate_response(result, @status)
  end

  # Method to perform multiplication
  def self.multiply(a, b)
    Rails.logger.info "Performing multiplication between #{a} and # #{b}"
    begin
      result = a * b
    rescue => err
      result = "cannot perform multiplication #{err}"
      @status = "failure"
    end
    return generate_response(result, @status)
  end

  # Method to perform division
  def self.divide(a, b)
    Rails.logger.info "Performing division between #{a} and # #{b}"
    if b!=0
      begin
        result = a.fdiv(b) # to return with decimal
      rescue => err
        result = "canot perform division #{err}"
        @status = "failure"
      end
    else
      result = "Cannot perform division by 0"
      @status = "failure"
    end
    return generate_response(result, @status)
  end

  # Method to perform module
  def self.module(a, b)
    Rails.logger.info "Performing mod between #{a} and # #{b}"
    begin
      result = a % b
    rescue => err
      result = "cannot perform mod #{err}"
      @status = "failure"
    end
    return generate_response(result, @status)
  end

  # Method to generate response
  def self.generate_response(result, status='success')
    response = {"status": status, "data": result}
    @status = 'success'
    return response
  end
end