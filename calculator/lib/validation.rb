class Validation
    def self.validate_data(params)
        is_data_valid = true
        operator = ""
        err_msg = ""
        if !params[:number1].present? || !params[:number2].present? || !params[:operation].present?
            is_valid_body = false
            err_msg = 'Invalid request body'
            return is_data_valid, operator, err_msg
        end
        if !params[:number1].is_a?(Numeric) || !params[:number2].is_a?(Numeric)
    
          is_data_valid = false
          err_msg = "Invalid parameter number1 '#{params[:number1]}' and/or number2 '#{params[:number2]}' both should be number"
          Rails.logger.error "Error in parameter body #{err_msg}"
          return is_data_valid, operator, err_msg
        end
        # operators = ['+', '-', '*', '%', '/']
        hash_operators = {"sum" => "+", "subtract" => '-', "multiply" => "*", "divide"=> "/", "module" => "%"}
        if hash_operators.has_value? params[:operation]
          operator = hash_operators.key(params[:operation])
          
        else
          err_msg = "Invalid operator, #{params[:operation]}"
          Rails.logger.error "Invalid operator, #{err_msg}"
          is_data_valid = false
          return is_data_valid, operator, err_msg
        end
        return is_data_valid, operator, err_msg
    end

    def self.validate_request_body(params)
        is_valid_body = true
        err_msg = ''
        if !params[:number1].present? || !params[:number2].present? || !params[:operation].present?
            is_valid_body = false
            err_msg = 'Invalid request body'
        end
        return is_valid_body, err_msg
    end

end