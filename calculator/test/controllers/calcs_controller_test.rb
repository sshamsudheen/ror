require "test_helper"

class CalcsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @calc = calcs(:one)
  end

  test "should get index" do
    get calcs_url, as: :json
    assert_response :success
  end

  test "should create calc" do
    assert_difference("Calc.count") do
      post calcs_url, params: { calc: { operation: @calc.operation } }, as: :json
    end

    assert_response :created
  end

  test "should show calc" do
    get calc_url(@calc), as: :json
    assert_response :success
  end

  test "should update calc" do
    patch calc_url(@calc), params: { calc: { operation: @calc.operation } }, as: :json
    assert_response :success
  end

  test "should destroy calc" do
    assert_difference("Calc.count", -1) do
      delete calc_url(@calc), as: :json
    end

    assert_response :no_content
  end
end
